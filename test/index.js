"use srict";
const assert = require('chai').assert,
    expect = require('chai').expect,
    path = require('path');

let AllEnv = require('../');


describe('Test: true', () => {
    it('Verify process.env integrity after persist', async () => {
        let prelng = Object.keys(process.env).length;
        let _folder = './test/2/'
        let allenv = new AllEnv({
            defaultPath: _folder,
            sources: ['dotenv', 'docker', 'appyaml']
        });
        let persisted = allenv.load();
        expect(Object.keys(process.env).length).to.be.equals(prelng + 1);
    });
    it('Default Configuration', async () => {
        let allenv = new AllEnv({});
        expect(allenv).to.be.instanceof(AllEnv);
        expect(allenv.sources).to.be.a('array');
        expect(allenv.sources).to.be.length(3);
        expect(allenv.sources[0]).to.be.equals('docker');
        expect(allenv.sources[1]).to.be.equals('appyaml');
        expect(allenv.sources[2]).to.be.equals('dotenv');
        expect(allenv.defaultPath).to.be.equals(path.resolve(__dirname, '../'));
        expect(allenv.dockerPath).to.be.equals(path.resolve(__dirname, '../', './Dockerfile'));
        expect(allenv.dotenvPath).to.be.equals(path.resolve(__dirname, '../', './.env'));
        expect(allenv.appyamlPath).to.be.equals(path.resolve(__dirname, '../', './app.yaml'));
        expect(allenv.ENV.persisted).to.be.undefined;
        allenv.load();
        expect(allenv.ENV.persisted).to.not.be.undefined;
        expect(allenv.env('docker')).to.be.undefined;
        expect(allenv.env('appyaml')).to.be.undefined;
        expect(allenv.env('dotenv')).to.be.undefined;
    });
    it('Reading Test with default configs, changed only path', async () => {
        let _folder = './test/1/'
        let allenv = new AllEnv({
            defaultPath: _folder
        });
        expect(allenv).to.be.instanceof(AllEnv);
        expect(allenv.sources).to.be.a('array');
        expect(allenv.sources).to.be.length(3);
        expect(allenv.sources[0]).to.be.equals('docker');
        expect(allenv.sources[1]).to.be.equals('appyaml');
        expect(allenv.sources[2]).to.be.equals('dotenv');
        expect(allenv.defaultPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}`));
        expect(allenv.dockerPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}Dockerfile`));
        expect(allenv.dotenvPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}.env`));
        expect(allenv.appyamlPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}app.yaml`));
        expect(allenv.ENV.persisted).to.be.undefined;
        let persisted = allenv.load();
        expect(allenv.ENV.persisted).to.not.be.undefined;
        expect(allenv.env('docker')).to.be.a('object');
        expect(allenv.env('appyaml')).to.be.a('object');
        expect(allenv.env('dotenv')).to.be.a('object');
        expect(allenv.env('docker').TEST_VAR).to.be.equals('3');
        expect(allenv.env('appyaml').TEST_VAR).to.be.equals('2');
        expect(allenv.env('dotenv').TEST_VAR).to.be.equals('1');
        expect(persisted.TEST_VAR).to.be.equals('1');
        expect(process.env.TEST_VAR).to.be.equals('1');
    });
    it('Check Overwriting Rules : only DockerFile', async () => {
        let _folder = './test/2/'
        let allenv = new AllEnv({
            defaultPath: _folder,
            sources: ['docker']
        });
        expect(allenv).to.be.instanceof(AllEnv);
        expect(allenv.sources).to.be.a('array');
        expect(allenv.sources).to.be.length(1);
        expect(allenv.sources[0]).to.be.equals('docker');
        expect(allenv.defaultPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}`));
        expect(allenv.dockerPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}Dockerfile`));
        expect(allenv.dotenvPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}.env`));
        expect(allenv.appyamlPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}app.yaml`));
        expect(allenv.ENV.persisted).to.be.undefined;
        let persisted = allenv.load();
        expect(allenv.ENV.persisted).to.not.be.undefined;
        expect(allenv.env('docker')).to.be.a('object');
        expect(allenv.env('appyaml')).to.be.undefined;
        expect(allenv.env('dotenv')).to.be.undefined;
        expect(allenv.env('docker').TEST_VAR).to.be.equals('3');
        expect(persisted.TEST_VAR).to.be.equals('3');
        expect(process.env.TEST_VAR).to.be.equals('3');
    });
    it('Check Overwriting Rules : only Appyaml', async () => {
        let _folder = './test/2/'
        let allenv = new AllEnv({
            defaultPath: _folder,
            sources: ['appyaml']
        });
        expect(allenv).to.be.instanceof(AllEnv);
        expect(allenv.sources).to.be.a('array');
        expect(allenv.sources).to.be.length(1);
        expect(allenv.sources[0]).to.be.equals('appyaml');
        expect(allenv.defaultPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}`));
        expect(allenv.dockerPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}Dockerfile`));
        expect(allenv.dotenvPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}.env`));
        expect(allenv.appyamlPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}app.yaml`));
        expect(allenv.ENV.persisted).to.be.undefined;
        let persisted = allenv.load();
        expect(allenv.ENV.persisted).to.not.be.undefined;
        expect(allenv.env('docker')).to.be.undefined;
        expect(allenv.env('appyaml')).to.be.a('object');
        expect(allenv.env('dotenv')).to.be.undefined;
        expect(allenv.env('appyaml').TEST_VAR).to.be.equals('2');
        expect(persisted.TEST_VAR).to.be.equals('2');
        expect(process.env.TEST_VAR).to.be.equals('2');
    });
    it('Check Overwriting Rules : only .env', async () => {
        let _folder = './test/2/'
        let allenv = new AllEnv({
            defaultPath: _folder,
            sources: ['dotenv']
        });
        expect(allenv).to.be.instanceof(AllEnv);
        expect(allenv.sources).to.be.a('array');
        expect(allenv.sources).to.be.length(1);
        expect(allenv.sources[0]).to.be.equals('dotenv');
        expect(allenv.defaultPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}`));
        expect(allenv.dockerPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}Dockerfile`));
        expect(allenv.dotenvPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}.env`));
        expect(allenv.appyamlPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}app.yaml`));
        expect(allenv.ENV.persisted).to.be.undefined;
        let persisted = allenv.load();
        expect(allenv.ENV.persisted).to.not.be.undefined;
        expect(allenv.env('docker')).to.be.undefined;
        expect(allenv.env('appyaml')).to.be.undefined;
        expect(allenv.env('dotenv')).to.be.a('object');
        expect(allenv.env('dotenv').TEST_VAR).to.be.equals('1');
        expect(persisted.TEST_VAR).to.be.equals('1');
        expect(process.env.TEST_VAR).to.be.equals('1');
    });
    it('Check Overwriting Rules woith all file types', async () => {
        let _folder = './test/2/'
        let allenv = new AllEnv({
            defaultPath: _folder,
            sources: ['dotenv', 'docker', 'appyaml']
        });
        expect(allenv).to.be.instanceof(AllEnv);
        expect(allenv.sources).to.be.a('array');
        expect(allenv.sources).to.be.length(3);
        expect(allenv.sources[0]).to.be.equals('dotenv');
        expect(allenv.sources[1]).to.be.equals('docker');
        expect(allenv.sources[2]).to.be.equals('appyaml');
        expect(allenv.defaultPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}`));
        expect(allenv.dockerPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}Dockerfile`));
        expect(allenv.dotenvPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}.env`));
        expect(allenv.appyamlPath).to.be.equals(path.resolve(__dirname, '../', `${_folder}app.yaml`));
        expect(allenv.ENV.persisted).to.be.undefined;
        let persisted = allenv.load();
        expect(allenv.ENV.persisted).to.not.be.undefined;
        expect(allenv.env('docker')).to.be.a('object');
        expect(allenv.env('appyaml')).to.be.a('object');
        expect(allenv.env('dotenv')).to.be.a('object');
        expect(allenv.env('docker').TEST_VAR).to.be.equals('3');
        expect(allenv.env('appyaml').TEST_VAR).to.be.equals('2');
        expect(allenv.env('dotenv').TEST_VAR).to.be.equals('1');
        expect(persisted.TEST_VAR).to.be.equals('2');
        expect(process.env.TEST_VAR).to.be.equals('2');
    });
});
