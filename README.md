#Allenv

[![NPM](https://nodei.co/npm/allenv.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/allenv/)

Loads environment variables from: .env file, Google Cloud Platform  app.yaml file and Docker file.

##Usage

**Import**

	const AllEnv = require('allenv');

**Default configs**


	let allenv = new AllEnv();
	allenv.load();


**Default with custom**

	
	let allenv = new AllEnv({
            defaultPath: '/your/custom/files/path',
            sources: ['dotenv', 'docker', 'appyaml']
        });
	allenv.load();
	
	

**Custom Files Paths**

	
	let allenv = new AllEnv({
            defaultPath: '/your/custom/files/path',
            dockerPath : '',
            dotenvPath : '',
            appyamlPath : ''
        });
	allenv.load();

	
##Overwriting

**Custom Overwriting example**

	let allenv = new AllEnv({
            defaultPath: '/your/custom/files/path',  
            sources: ['appyaml','docker']
        });
	allenv.load();