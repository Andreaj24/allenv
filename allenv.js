'use strict';
const fs = require('fs'),
    path = require('path');
const yamlParser = require('yaml'),
    dockerParser = require('docker-file-parser'),
    dotenv = require('dotenv'),
    dotenvParser = dotenv.parse;

/**
 * @class
 * @classdesc                                   [[Description]]
 * @params
 *        dockerPath   {String}   Custom File Path/name
 *        dotenvPath   {String}   Custom File Path/name
 *        appyamlPath  {String}   Custom File Path/name
 *
 *        sources    {Array}    List of predefined keywords (docker,dotenv,appyaml)
 *                              which determines the order of reading and overwriting
 */
class AllEnv {
    constructor(params = {}, debug = false) {
        this.debug = debug;
        this.defaultPath = path.resolve(__dirname, './../../', params.defaultPath ? params.defaultPath : './');
        this.dockerPath = path.resolve(this.defaultPath, params.dockerPath ? params.dockerPath : './Dockerfile');
        this.dotenvPath = path.resolve(this.defaultPath, params.dotenvPath ? params.dotenvPath : './.env');
        this.appyamlPath = path.resolve(this.defaultPath, params.appyamlPath ? params.appyamlPath : './app.yaml');
        this.sources = params.sources ? params.sources : ['docker', 'appyaml', 'dotenv'];
        this.parser = {
            dotenv: dotenvParser,
            docker: dockerParser,
            appyaml: yamlParser
        }
        this.ENV = {};
    }
    loadEnvs_appyaml() {
        this.ENV.appyaml = {};
        let appyamlFile = fs.readFileSync(this.appyamlPath, 'utf8');
        Object.assign(this.ENV.appyaml, yamlParser.parse(appyamlFile).env_variables);
    }
    loadEnvs_dotenv() {
        this.ENV.dotenv = {};
        let dotenvFile = fs.readFileSync(this.dotenvPath, 'utf8');
        const buf = Buffer.from(dotenvFile);
        Object.assign(this.ENV.dotenv, dotenvParser(buf));
    }

    loadEnvs_docker() {
        this.ENV.docker = {};
        let dockerFile = fs.readFileSync(this.dockerPath, 'utf8');
        let commands = dockerParser.parse(dockerFile, {
            includeComments: false
        });
        commands.forEach((cmd) => {
            if (cmd.name == 'ENV')
                Object.assign(this.ENV.docker, cmd.args);
        });
    }

    checkFileExists(name) {
        let p = this[`${name}Path`];
        return fs.existsSync(p)
    }

    parse() {
        for (let env of this.sources) {
            if (this.checkFileExists(env))
                this[`loadEnvs_${env}`]();
        }
    }
    log() {
        if (!this.debug) return;
        console.log(`allenv|>           Files`);
        console.log(`allenv|>           sources: ${this.sources}`);
        console.log(`allenv|>           defaultPath: ${this.defaultPath}`);
        console.log(`allenv|>           dockerPath: ${this.dockerPath}`);
        console.log(`allenv|>           dotenvPath: ${this.dotenvPath}`);
        console.log(`allenv|>           appyamlPath: ${this.appyamlPath}`);
        for (let env of this.sources) {
            console.log(`allenv|>           env: ${env}`);
            for (let key in this.ENV[env]) {
                console.table({
                    [key]: this.ENV[env][key]
                });
            }
        }
        console.log(`allenv|>           env: persisted`);
        console.log(JSON.stringify(this.ENV.persisted, 0, 2))
    }
    persist() {
        let _env = {};
        for (let env of this.sources) Object.assign(_env, this.ENV[env]);
        Object.assign(process.env, _env);
        this.ENV.persisted = _env;
        return _env;
    }
    load() {
        this.parse();
        let persisted = this.persist();
        this.log();
        return persisted;
    }


    env(name = null) {
        if (!name) return this.ENV['persisted'];
        return this.ENV[name];
    }
}


module.exports = AllEnv;
